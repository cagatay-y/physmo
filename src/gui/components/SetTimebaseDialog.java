/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.components;

import javax.swing.JDialog;
import javax.swing.JFormattedTextField;

import java.awt.event.ActionListener;
import java.text.DecimalFormat;

import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import main.PhysMo;

/**
 * 
 *
 * @author jasonkb
 */
public class SetTimebaseDialog extends JDialog implements ActionListener {
	private JPanel myPanel = null;
	private JButton yesButton = null;

	private DecimalFormat timebaseFormat;
	JFormattedTextField t;

	SetTimebaseDialog(JFrame frame, boolean modal, String myMessage) {
		super(frame, modal);

		myPanel = new JPanel();

		myPanel.add(new JLabel("FPS: "));

		t = new JFormattedTextField(timebaseFormat);
		t.setValue(PhysMo.video.getFps());

		myPanel.add(t);
		yesButton = new JButton("Set new timebase");
		yesButton.addActionListener(this);
		myPanel.add(yesButton);

		getContentPane().add(myPanel);

		pack();
		setLocationRelativeTo(frame);
		setVisible(true);
	}

	public void actionPerformed(ActionEvent e) {
		if (yesButton == e.getSource()) {

			PhysMo.video.setFps((double) t.getValue());

			setVisible(false);
		}

	}
}
