/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.components;

import tools.*;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import main.PhysMo;
import main.Video;

/**
 * 
 *
 * @author jasonkb
 */
public class FirstScreen extends JPanel implements WindowListener, Runnable {
	public JButton newProject;
	public JButton existingProject;
	public JButton howtoUse;
	public JButton aboutPhysmo;
	public JFrame f;
	public JProgressBar pb;

	public AnalysisWindow projectWindow;

	public FirstScreen(JFrame frame) {
		pb = new JProgressBar(JProgressBar.HORIZONTAL, 0, 10);
		f = frame;
		f.addWindowListener(this);
		this.setLayout(new FlowLayout());
		newProject = new JButton("Open Video");
		existingProject = new JButton("Open Project");
		aboutPhysmo = new JButton("About PhysMo");
		howtoUse = new JButton("Instructions");

		this.add(newProject);
		// this.add(existingProject);
		this.add(aboutPhysmo);
		this.add(howtoUse);

		this.add(pb);

		// pb.setVisible(false);

		newProject.addActionListener(e -> openNewProject());
		aboutPhysmo.addActionListener(e -> showAboutFile());
		f.pack();
	}

	void openNewProject() {
		JFileChooser videoSelect;
		videoSelect = new JFileChooser(PhysMo.prefs.get("last_video_directory", "./"));
		videoSelect.setFileFilter(
				new FileNameExtensionFilter("Video file", "mov", "avi", "mpg", "mpeg", "wmv", "rm", "flv", "3gp"));
		videoSelect.setFileView(new MovieFileView());
		videoSelect.setAcceptAllFileFilterUsed(false);
		videoSelect.setDialogTitle("Open a video file...");

		int retval = videoSelect.showOpenDialog(null);
		if (retval == JFileChooser.APPROVE_OPTION) {
			// ... The user selected a file, get it, use it.
			File file = videoSelect.getSelectedFile();
			PhysMo.prefs.put("last_video_directory", file.getParentFile().getAbsolutePath());

			// ... Update user interface.
			f.setTitle("PhysMo v2.0 - " + file.getName());
			PhysMo.video = new Video(file.getPath());

			// parse the video information
			double[] videoProperties = FFmpegIO.getProperties(PhysMo.video.getAbsolutePath());
			PhysMo.video.setDuration(videoProperties[0]);
			PhysMo.video.setFps(videoProperties[1]);

			f.repaint();
			// select working directory

			JFileChooser workingDirectorySelect;

			workingDirectorySelect = new JFileChooser(PhysMo.prefs.get("last_working_directory", "./"));
			workingDirectorySelect.setDialogTitle("Create a directory to work in...");
			workingDirectorySelect.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

			retval = workingDirectorySelect.showOpenDialog(null);
			if (retval == JFileChooser.APPROVE_OPTION) {
				// ... The user selected a file, get it, use it.
				file = workingDirectorySelect.getSelectedFile();
				// ... Update user interface.
				PhysMo.workingDirectory = file.getAbsolutePath();
				PhysMo.prefs.put("last_working_directory", file.getAbsolutePath());

				newProject.setEnabled(false);

				f.setVisible(true);

				Thread t = new Thread(this);

				t.start();
				synchronized (this) {
					notifyAll();
				}
			}
		}
	}

	void showAboutFile() {
		try {
			Desktop.getDesktop().open(new File("./about.pdf"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	public void run() {

		pb.setIndeterminate(true);
		pb.setString("Decomposing video");
		pb.setStringPainted(true);
		f.repaint();

		FFmpegIO.decomposeVideo(PhysMo.video.getAbsolutePath(), PhysMo.workingDirectory, "png");

		pb.setIndeterminate(true);
		pb.setString("Opening video");
		pb.setStringPainted(true);
		f.repaint();

		try {
			Runnable doWorkRunnable3 = new Runnable() {
				public void run() {
					projectWindow = new AnalysisWindow();
					projectWindow.setVisible(true);
					projectWindow.toFront();
					projectWindow.setExtendedState(projectWindow.getExtendedState() | JFrame.MAXIMIZED_BOTH);
					PhysMo.isVisible = true;

				}
			};
			SwingUtilities.invokeAndWait(doWorkRunnable3);
		} catch (Exception e) {
			e.printStackTrace();
		}
		projectWindow.addWindowListener(this);

		pb.setIndeterminate(false);
		pb.setStringPainted(false);
		f.repaint();
		f.toBack();

	}

	public void windowOpened(WindowEvent e) {

	}

	public void windowClosing(WindowEvent e) {
		if (e.getSource() == projectWindow) {
			System.out.println("Window closing");
			PhysMo.isVisible = false;
			newProject.setEnabled(true);
			newProject.repaint();
		}

		if (e.getSource() == f) {
			int i = JOptionPane.showConfirmDialog(null, "This will close PhysMo! \nAre you sure?", "Close PhysMo",
					JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
			if (i == JOptionPane.YES_OPTION) {
				f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			} else {
				f.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
			}
		}

	}

	public void windowClosed(WindowEvent e) {
		System.out.println("Window closed");
	}

	public void windowIconified(WindowEvent e) {

	}

	public void windowDeiconified(WindowEvent e) {

	}

	public void windowActivated(WindowEvent e) {

	}

	public void windowDeactivated(WindowEvent e) {

	}

}
