/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.components;

import gui.images.Distributor;
import javax.swing.*;
import java.awt.event.*;

/**
 * 
 *
 * @author jasonkb
 */
public class AnalysisWindowMenuBar implements ActionListener {
	JMenuBar mb;

	JMenu fileMenu;
	JMenuItem closeWindow;

	JMenu videoMenu;
	JMenuItem setTimebase;
	JMenuItem videoProperties;

	JMenu reportMenu;
	JMenuItem spreadsheetReport;

	public AnalysisWindowMenuBar() {
		mb = new JMenuBar();

		fileMenu = new JMenu("File");
		fileMenu.setMnemonic('F');
		mb.add(fileMenu);

		closeWindow = new JMenuItem("Close Analysis Window", Distributor.getCloseIcon());
		closeWindow.setMnemonic('C');
		closeWindow.addActionListener(this);
		fileMenu.add(closeWindow);

		videoMenu = new JMenu("Video");
		videoMenu.setMnemonic('V');

		setTimebase = new JMenuItem("Set Timebase", Distributor.getTimebase());
		setTimebase.setMnemonic('T');
		setTimebase.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, ActionEvent.CTRL_MASK));
		setTimebase.addActionListener(this);
		videoMenu.add(setTimebase);
		videoMenu.addSeparator();

		videoProperties = new JMenuItem("Video Information", Distributor.getQuery());
		videoProperties.setMnemonic('I');
		videoProperties.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, ActionEvent.CTRL_MASK));
		videoProperties.addActionListener(this);
		videoMenu.add(videoProperties);

		mb.add(videoMenu);

		reportMenu = new JMenu("Report");
		reportMenu.setMnemonic('R');

		spreadsheetReport = new JMenuItem("Export Data to Spreadsheet");
		spreadsheetReport.setMnemonic('E');
		spreadsheetReport.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.CTRL_MASK));
		spreadsheetReport.addActionListener(this);
		reportMenu.add(spreadsheetReport);

		mb.add(reportMenu);

	}

	public JMenuBar getJMenuBar() {
		return this.mb;
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == setTimebase) {
			new SetTimebaseDialog(null, true, "Set FPS");
			this.getJMenuBar().firePropertyChange("timebaseUpdate", -1, 0);
		}
		if (e.getSource() == videoProperties) {
			new VideoInformationDialog(null, true);
		}
		if (e.getSource() == closeWindow) {
			this.getJMenuBar().firePropertyChange("closeWindow", -1, 0);
		}

		if (e.getSource() == spreadsheetReport) {
			this.getJMenuBar().firePropertyChange("spreadsheetReport", -1, 0);
		}

	}
}
