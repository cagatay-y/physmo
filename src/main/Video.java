package main;

import java.io.File;

public class Video extends File {
	String information = "";
	double duration = 0.0;
	double fps = 0.0;

	public Video(String pathname) {
		super(pathname);

	}

	public String getInformation() {
		return information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

	public double getDuration() {
		return duration;
	}

	public void setDuration(double duration) {
		this.duration = duration;
	}

	public double getFps() {
		return fps;
	}

	public void setFps(double fps) {
		this.fps = fps;
	}
}
