//This is the main window to collect all components and tools in the same location
package main;

import gui.images.*;
import gui.components.*;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.prefs.Preferences;

/**
 *
 * @author jasonkb
 */
public class PhysMo extends JFrame {
	public FirstScreen openScreen;
	public static final Preferences prefs = Preferences.userNodeForPackage(PhysMo.class);
	public static String workingDirectory = "";
	public static double unMagnifiedRealWorldDistance = 0.0;
	public static boolean windows;

	public static Video video;

	public static boolean isVisible = false;

	public PhysMo() {
		super("PhysMo v2.0");
		super.setIconImage(Distributor.getFrameIcon());
		openScreen = new FirstScreen(this);
		SwingUtilities.updateComponentTreeUI(openScreen);
		this.add(openScreen, BorderLayout.CENTER);
		this.pack();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension fSize = this.getSize();
		int x = (screen.width - fSize.width) / 2;
		int y = (screen.height - fSize.height) / 2;
		this.setLocation(x, y);

		PhysMo.windows = isWindows();
	}

	public static boolean isWindows() {
		String os = System.getProperty("os.name").toLowerCase();
		// windows
		return (os.indexOf("win") >= 0);
	}

	public static void main(String args[]) {
		PhysMo f = new PhysMo();
		SwingUtilities.updateComponentTreeUI(f);

		f.setVisible(true);
	}
}
