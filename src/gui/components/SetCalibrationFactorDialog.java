/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.components;

import javax.swing.JDialog;
import javax.swing.JFormattedTextField;

import java.text.DecimalFormat;

import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import main.PhysMo;

/**
 * 
 *
 * @author jasonkb
 */
public class SetCalibrationFactorDialog extends JDialog {
	private JPanel myPanel = null;
	private JButton yesButton = null;

	private double calibrationFactor;
	private DecimalFormat factorFormat;
	JFormattedTextField t;

	SetCalibrationFactorDialog(JFrame frame, boolean modal, String myMessage) {
		super(frame, modal);

		this.calibrationFactor = 0.00;
		myPanel = new JPanel();

		myPanel.add(new JLabel("Distance (m): "));

		t = new JFormattedTextField(factorFormat);
		t.setValue(PhysMo.video.getFps());

		myPanel.add(t);
		yesButton = new JButton("Set calibration");
		yesButton.addActionListener(e -> updateCalibration());
		myPanel.add(yesButton);

		getContentPane().add(myPanel);

		pack();
		setLocationRelativeTo(frame);
		setVisible(true);
	}

	public double getCalibration() {
		return this.calibrationFactor;
	}

	void updateCalibration() {
		this.calibrationFactor = (double) t.getValue();
		System.out.println("The cal factor post dialog is: " + this.calibrationFactor);
		setVisible(false);
	}
}
