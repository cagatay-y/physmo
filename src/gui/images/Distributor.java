package gui.images;

import java.awt.*;
import java.net.*;

import javax.swing.ImageIcon;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 
 *
 * @author jasonkb
 */
public class Distributor {
	static ImageIcon getIcon(String iconName) {
		URL urlImage = Distributor.class.getResource(iconName);
		ImageIcon img = new ImageIcon(urlImage);
		return img;
	}

	static public ImageIcon getCloseIcon() {
		return getIcon("cross.png");
	}

	static public ImageIcon getPhotoFileIcon() {
		return getIcon("photo.png");
	}

	static public ImageIcon getVideoIcon() {
		return getIcon("film.png");
	}

	static public ImageIcon getSnapshot() {
		return getIcon("camera.png");
	}

	static public ImageIcon getSave() {
		return getIcon("disk.png");
	}

	static public ImageIcon getMarker() {
		return getIcon("marker.png");
	}

	static public ImageIcon getQuery() {
		return getIcon("query.png");
	}

	static public ImageIcon getTimebase() {
		return getIcon("timebase.png");
	}

	static public ImageIcon getTarget() {
		return getIcon("target.png");
	}

	static public ImageIcon getProtractor() {
		return getIcon("angle.png");
	}

	static public ImageIcon getRuler() {
		return getIcon("ruler.png");
	}

	static public Image getFrameIcon() {
		URL urlImage = Distributor.class.getResource("frameIcon.png");
		Image img = Toolkit.getDefaultToolkit().getImage(urlImage);
		return img;
	}

	static public ImageIcon getFF() {
		return getIcon("ff.png");
	}

	static public ImageIcon getRW() {
		return getIcon("rw.png");
	}

	static public ImageIcon getPlay() {
		return getIcon("play.png");
	}

	static public ImageIcon getPause() {
		return getIcon("pause.png");
	}

	static public ImageIcon getStop() {
		return getIcon("stop.png");
	}

	static public ImageIcon getEnd() {
		return getIcon("end.png");
	}

	static public ImageIcon getStart() {
		return getIcon("start.png");
	}
}